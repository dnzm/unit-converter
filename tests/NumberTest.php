<?php
declare(strict_types=1);

namespace Dnzm\UnitConverter;

class NumberTest extends \PHPUnit\Framework\TestCase
{
    public function testNumberCanBeInstanciated()
    {
        $n = new Number(10, 'm');
        self::assertInstanceOf(Number::class, $n);
    }

    /**
     * @dataProvider numberProvider
     */
    public function testNumberCanStringify($number, string $unit, ?string $prefix, string $toString, string $expectedUnit, string $expectedPrefix)
    {
        $n = new Number($number, $unit, $prefix);
        self::assertEquals($toString, (string) $n);
    }

    public function testNumbersCanChange()
    {
        $n = (new Number(1, 'm'))->withNumber(2);
        self::assertEquals("2 m", (string) $n);
    }
    /**
     * @dataProvider numberProvider
     */
    public function testNumbersGetUnits($number, string $unit, ?string $prefix, string $toString, string $expectedUnit, string $expectedPrefix)
    {
        $n = new Number($number, $unit, $prefix);
        self::assertEquals($number, $n->getNumber());
        self::assertEquals($expectedUnit, $n->getUnit());
        self::assertEquals($expectedPrefix, $n->getPrefix());
    }

    public function testInvalidNumericThrowsException()
    {
        self::expectException(Exception::class);
        $n = new Number('foo', 'm');
    }

    public function testSettingInvalidNumericThrowsException()
    {
        self::expectException(Exception::class);
        $n = (new Number(1000, 'm'))->withNumber('foo');
    }

    public function testInvalidPrefixThrowsException()
    {
        self::expectException(Exception::class);
        $n = new Number(100, 'unit', 'x');
    }

    public function numberProvider(): array
    {
        return [
            '10 meters' => [10, 'm', null, '10 m', 'm', ''],
            '10.1 meters' => [10.1, 'm', null, '10.1 m', 'm', ''],
            '10 kilometers' => [10, 'km', null, '10 km', 'm', 'k'],
            '1 megapuppy' => [1, 'mpuppy', null, '1 mpuppy', 'puppy', 'm'],
            '1 mom' => [1, 'mom', '', '1 mom', 'mom', ''],
            '1 decameter' => [1, 'dam', null, '1 dam', 'm', 'da'],
            '1 qubit' => [1, 'qubit', null, '1 qubit', 'qubit', ''],
        ];
    }
}
