<?php
declare(strict_types=1);

namespace Dnzm\UnitConverter;

class PrefixTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider validPrefixDataProvider
     * @dataProvider invalidPrefixDataProvider
     */
    public function testIsPrefix(string $prefix, bool $expected)
    {
        self::assertEquals($expected, Prefix::isPrefix($prefix));
    }

    /**
     * @dataProvider invalidPrefixDataProvider
     */
    public function testInvalidFromPrefixThrowsException(string $prefix, bool $isValid)
    {
        self::expectException(Exception::class);
        Prefix::getTranslationFactor($prefix, Prefix::SI_NONE);
    }

    /**
     * @dataProvider invalidPrefixDataProvider
     */
    public function testInvalidToPrefixThrowsException(string $prefix, bool $isValid)
    {
        self::expectException(Exception::class);
        Prefix::getTranslationFactor(Prefix::SI_NONE, $prefix);
    }

    /**
     * @dataProvider validPrefixDataProvider
     */
    public function testGetFactor(string $prefix, bool $isValid, int $factor)
    {
        self::assertEquals($factor, Prefix::getFactor($prefix));
    }

    /**
     * @dataProvider translationFactorDataProvider
     */
    public function testGetTranslationFactor(string $from, string $to, int $expected)
    {
        self::assertEquals($expected, Prefix::getTranslationFactor($from, $to));
    }

    public function validPrefixDataProvider(): array
    {
        return [
            'k is valid prefix' => [Prefix::SI_KILO, true, 3],
            'µ is valid prefix' => [Prefix::SI_MICRO, true, -6],
            'none is valid prefix' => [Prefix::SI_NONE, true, 0],
        ];
    }

    public function invalidPrefixDataProvider(): array
    {
        return [
            'X is invalid prefix' => ['X', false],
            '3 is invalid prefix' => [3, false],
        ];
    }

    public function translationFactorDataProvider(): array
    {
        return [
            'between base and kilo: -3' => [Prefix::SI_NONE, Prefix::SI_KILO, -3],
            'between kilo and base: 3' => [Prefix::SI_KILO, Prefix::SI_NONE, 3],
            'between milli and kilo: -6' => [Prefix::SI_MILLI, Prefix::SI_KILO, -6],
            'between kilo and kilo: 0' => [Prefix::SI_KILO, Prefix::SI_KILO, 0],
        ];
    }
}

