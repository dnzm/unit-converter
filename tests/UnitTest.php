<?php
declare(strict_types=1);

namespace Dnzm\UnitConverter;

use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testCanConvertGramsToTonnes()
    {
        self::assertSame(1.0, Unit::convert(Unit::UNIT_GRAM, Unit::UNIT_TONNE, 1000000));
        self::assertSame(10.0, (new Number(10000, 'kg'))->toUnit(Unit::UNIT_TONNE)->getNumber());
    }

    public function testCanConvertTonnesToGrams()
    {
        self::assertSame(1000000.0, Unit::convert(Unit::UNIT_TONNE, Unit::UNIT_GRAM, 1));
        self::assertSame(10000.0, (new Number(10, 't'))->toUnit(Unit::UNIT_GRAM)->withPrefix(Prefix::SI_KILO)->getNumber());
    }

    public function testFromInvalidUnitThrowsException()
    {
        self::expectException(Exception::class);
        Unit::convert(Unit::UNIT_GRAM, 'foo', 1);
    }

    public function testToInvalidUnitThrowsException()
    {
        self::expectException(Exception::class);
        Unit::convert(Unit::UNIT_GRAM, 'foo', 1);
    }
}
