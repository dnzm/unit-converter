<?php
declare(strict_types=1);

use Dnzm\UnitConverter\Number;
use Dnzm\UnitConverter\Prefix;
use Dnzm\UnitConverter\Unit;

require_once __DIR__ . '/vendor/autoload.php';

// Start with an amount of grams
$n = new Number(1000, 'g');
echo "n=$n\n";
echo "number={$n->getNumber()}\n";
echo "prefix=" . $n->getPrefix() . "\n";
echo "unit=" . $n->getUnit() . "\n\n";

// ... we want those as kilograms
$n = $n->withPrefix(Prefix::SI_KILO);
echo "n=$n\n";
echo "number={$n->getNumber()}\n";
echo "prefix=" . $n->getPrefix() . "\n";
echo "unit=" . $n->getUnit() . "\n\n";

// ... or no, wait, we want micrograms
$n = $n->withPrefix(Prefix::SI_MICRO);
echo "n=$n\n";

// Actually, we got the number wrong and wanted to know tonnes.
$n = $n->withPrefix(Prefix::SI_KILO)
    ->withNumber(1200000);
echo "n=$n\n";
$n = $n->toUnit(Unit::UNIT_TONNE);
echo "n=$n\n";

// That was a big number
$n = $n->withPrefix(Prefix::SI_KILO);
echo "n=$n\n";

// And back to kilos
$n = $n->toUnit(Unit::UNIT_GRAM)->withPrefix(Prefix::SI_KILO);
echo "n=$n\n";

// Trying to convert it to a non-supported unit should go kaboom
$n->toUnit('furlongs');


