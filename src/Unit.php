<?php
declare(strict_types=1);

namespace Dnzm\UnitConverter;

class Unit
{
    const UNIT_GRAM = 'g';
    const UNIT_TONNE = 't';
    
    const UNITS = [
        self::UNIT_GRAM => [
            self::UNIT_TONNE => 1/1000000,
        ],
    ];

    public static function convert(string $fromUnit, string $toUnit, float $number): float
    {
        if (!isset(self::UNITS[$fromUnit][$toUnit]) && !isset(self::UNITS[$toUnit][$fromUnit])) {
            throw Exception::invalidConversionUnit($fromUnit, $toUnit);
        }

        $factor = self::UNITS[$fromUnit][$toUnit] ?? 1 / self::UNITS[$toUnit][$fromUnit];

        return $number * $factor;
    }
}
