<?php
declare(strict_types=1);

namespace Dnzm\UnitConverter;

/**
 * @codeCoverageIgnore
 */
class Exception extends \Exception
{
    public static function inputIsNotNumeric($number): Exception
    {
        return new self("Expected number; got '$number'", 1);
    }

    public static function invalidPrefix($prefix): Exception
    {
        return new self("'$prefix' is not a known SI Prefix", 2);
    }

    public static function invalidConversionUnit(string $fromUnit, string $toUnit): Exception
    {
        return new self("Cannot convert from '$fromUnit' to '$toUnit'");
    }
}
