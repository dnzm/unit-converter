<?php
declare(strict_types=1);

namespace Dnzm\UnitConverter;

/**
 * @see https://en.wikipedia.org/wiki/Metric_prefix
 */
class Prefix
{
    const SI_YOTTA = 'Y';
    const SI_ZETTA = 'Z';
    const SI_EXA = 'E';
    const SI_PETA = 'P';
    const SI_TERA = 'T';
    const SI_GIGA = 'G';
    const SI_MEGA = 'M';
    const SI_KILO = 'k';
    const SI_HECTO = 'h';
    const SI_DECA = 'da';
    const SI_NONE = '';
    const SI_DECI = 'd';
    const SI_CENTI = 'c';
    const SI_MILLI = 'm';
    const SI_MICRO = 'μ';
    const SI_NANO = 'n';
    const SI_PICO = 'p';
    const SI_FEMTO = 'f';
    const SI_ATTO = 'a';
    const SI_ZEPTO = 'z';
    const SI_YOCTO = 'y';

    const SI_YOTTA_POWER = 24;
    const SI_ZETTA_POWER = 21;
    const SI_EXA_POWER = 18;
    const SI_PETA_POWER = 15;
    const SI_TERA_POWER = 12;
    const SI_GIGA_POWER = 9;
    const SI_MEGA_POWER = 6;
    const SI_KILO_POWER = 3;
    const SI_HECTO_POWER = 2;
    const SI_DECA_POWER = 1;
    const SI_NONE_POWER = 0;
    const SI_DECI_POWER = -1;
    const SI_CENTI_POWER = -2;
    const SI_MILLI_POWER = -3;
    const SI_MICRO_POWER = -6;
    const SI_NANO_POWER = -9;
    const SI_PICO_POWER = -12;
    const SI_FEMTO_POWER = -15;
    const SI_ATTO_POWER = -18;
    const SI_ZEPTO_POWER = -21;
    const SI_YOCTO_POWER = -24;

    const SI_PREFIXES = [
        self::SI_YOTTA => self::SI_YOTTA_POWER,
        self::SI_ZETTA => self::SI_ZETTA_POWER,
        self::SI_EXA => self::SI_EXA_POWER,
        self::SI_PETA => self::SI_PETA_POWER,
        self::SI_TERA => self::SI_TERA_POWER,
        self::SI_GIGA => self::SI_GIGA_POWER,
        self::SI_MEGA => self::SI_MEGA_POWER,
        self::SI_KILO => self::SI_KILO_POWER,
        self::SI_HECTO => self::SI_HECTO_POWER,
        self::SI_DECA => self::SI_DECA_POWER,
        self::SI_NONE => self::SI_NONE_POWER,
        self::SI_DECI => self::SI_DECI_POWER,
        self::SI_CENTI => self::SI_CENTI_POWER,
        self::SI_MILLI => self::SI_MILLI_POWER,
        self::SI_MICRO => self::SI_MICRO_POWER,
        self::SI_NANO => self::SI_NANO_POWER,
        self::SI_PICO => self::SI_PICO_POWER,
        self::SI_FEMTO => self::SI_FEMTO_POWER,
        self::SI_ATTO => self::SI_ATTO_POWER,
        self::SI_ZEPTO => self::SI_ZEPTO_POWER,
        self::SI_YOCTO => self::SI_YOCTO_POWER,
    ];

    public static function isPrefix(string $q): bool
    {
        return in_array($q, array_keys(self::SI_PREFIXES));
    }

    public static function getFactor(string $prefix): int
    {
        return self::SI_PREFIXES[$prefix];
    }

    public static function getTranslationFactor(string $fromPrefix, string $toPrefix): int
    {
        if (!self::isPrefix($fromPrefix)) {
            throw Exception::invalidPrefix($fromPrefix);
        }
        if (!self::isPrefix($toPrefix)) {
            throw Exception::invalidPrefix($toPrefix);
        }

        return self::getFactor($fromPrefix) - self::getFactor($toPrefix);
    }
}
