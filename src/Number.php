<?php
declare(strict_types=1);

namespace Dnzm\UnitConverter;

class Number
{
    protected $number;
    protected $unit;
    protected $prefix;

    public function __construct($number, string $unit, ?string $prefix = null)
    {
        if (!is_numeric($number)) {
            throw Exception::inputIsNotNumeric($number);
        }

        if ($prefix !== null && !in_array($prefix, array_keys(Prefix::SI_PREFIXES))) {
            throw Exception::invalidPrefix($prefix);
        }

        if ($prefix === null) {
            $prefix = $this->autoDetectPrefixFromUnit($unit);
        }

        $unit = $this->subtractPrefixFromUnit($unit, $prefix);

        $this->number = $number;
        $this->unit = $unit;
        $this->prefix = $prefix;
    }

    protected function autoDetectPrefixFromUnit(string $unit)
    {
        $strlen = strlen($unit);
        if ($strlen < 2) {
            return '';
        }

        if ($strlen >= 3 && stripos($unit, Prefix::SI_DECA) === 0) {
            return Prefix::SI_DECA;
        }

        $substr = substr($unit, 0, 1);
        if ($strlen >= 2 && Prefix::isPrefix($substr)) {
            return $substr;
        }

        return '';
    }

    protected function subtractPrefixFromUnit(string $unit, string $prefix)
    {
        $prefixLength = strlen($prefix);
        if ($prefixLength === 0) {
            return $unit;
        }

        if (substr($unit, 0, $prefixLength) === $prefix) {
            $unit = substr($unit, $prefixLength);
        }

        return $unit;
    }

    public function __toString()
    {
        return "{$this->number} {$this->prefix}{$this->unit}";
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function withPrefix(string $prefix): self
    {
        $number = $this->number;
        if ($prefix !== $this->prefix) {
            $factor = 10 ** Prefix::getTranslationFactor($this->prefix, $prefix);
            $number *= $factor;
        }

        return new self($number, $this->unit, $prefix);
    }

    public function toUnit(string $toUnit): self
    {
        $number = $this->withPrefix(Prefix::SI_NONE);
        $number = Unit::convert($this->unit, $toUnit, $number->getNumber());

        return new self($number, $toUnit);
    }

    public function withNumber($number): self
    {
        if (!is_numeric($number)) {
            throw Exception::inputIsNotNumeric($number);
        }

        return new self($number + 0, $this->unit, $this->prefix);
    }
}

